$(document).ready(function(){

    $("#preloader").delay(500).fadeOut("slow");

    $(".owl-carousel").owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        navContainerClass:'navigator',
        navText: ["<img src='img/2-layers.svg'>","<img src='img/next.svg'>"],
        items:1
    });
    // Add smooth scrolling on all links inside the navbar
    $(".navbar a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {

            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function(){

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });

        } // End if

    });

    $('.hamburger').click(function () {
      $(this).toggleClass('is-active');
    });
    $('.to-top').toTop({
        right: 25,
    });
    lightbox.option({
        'alwaysShowNavOnTouchDevices': true,
        'wrapAround': true,
        'showImageNumberLabel': false
    })
});